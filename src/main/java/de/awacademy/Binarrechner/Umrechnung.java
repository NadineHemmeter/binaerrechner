package de.awacademy.Binarrechner;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Umrechnung {

    @NotEmpty
    @Pattern(message = "Bitte eine gültige Binärzahl eingeben", regexp = "[0-1]{4}")
    private String zahl;

    public String getZahl() {
        return zahl;
    }

    public void setZahl(String zahl) {
        this.zahl = zahl;
    }

}
