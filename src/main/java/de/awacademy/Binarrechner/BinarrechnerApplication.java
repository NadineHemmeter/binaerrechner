package de.awacademy.Binarrechner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BinarrechnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinarrechnerApplication.class, args);
	}

}
