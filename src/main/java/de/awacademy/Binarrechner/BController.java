package de.awacademy.Binarrechner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class BController {
    private ServiceUmrechnung serviceUmrechnung;

    @Autowired
    public BController(ServiceUmrechnung serviceUmrechnung){
        this.serviceUmrechnung = serviceUmrechnung;
    }

    @GetMapping("/Startseite")
    public String startseite(Model model){
        model.addAttribute("umrechnung", new Umrechnung());
        return "Startseite";
    }

    @PostMapping("/Startseite")
    public String ergebnis(@Valid @ModelAttribute Umrechnung umrechnung, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "Startseite";
        }
        else {
            int zahl = serviceUmrechnung.dezimal(serviceUmrechnung.umrechnung(umrechnung));
            model.addAttribute("Ergebnis", zahl);
            return "Ergebnis";
        }
    }
}
