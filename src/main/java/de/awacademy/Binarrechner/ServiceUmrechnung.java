package de.awacademy.Binarrechner;

import org.springframework.stereotype.Service;

@Service
public class ServiceUmrechnung {
    public int[] umrechnung(Umrechnung umrechnung){
        char[] array = new char[umrechnung.getZahl().length()];
        for(int i = 0; i<= array.length-1;i++){
            array[i] = umrechnung.getZahl().charAt(i);
        }

        int[] array2 = new int[array.length];
        for(int i = 0; i<= array.length-1; i++){
            array2[i] = Integer.parseInt(String.valueOf(array[i]));
        }

        return array2;
    }

    public int dezimal(int[] array){
        int summe = 0;
        for(int i = 0; i <= array.length-1; i++){
            summe += (Math.pow(2.0,(array.length-1-i)))*array[i];
        }
        return summe;
    }
}
